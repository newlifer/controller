#pragma once
#include <atomic>

#include <gpio_pins.hpp>

namespace yarildo
{
namespace molding
{
namespace service
{

        /** @brief Main application class
         * 
         */
        class Application {
        public:
        
                static Application& instance()
                {
                        static Application inst;

                        return inst;
                }

                /** @brief Inits all peripheral interfaces
                 * @details
                 */
                void init_peripheral();

                /** @brief Runs main loop
                 */
                int cycle();

                void gpio_b_handle();

        private:

                Application() = default;
                ~Application() = default;
                
                /** @brief Inits clock to default value.
                 */
                void init_clock();
                void init_gpio_input();
                void init_gpio_output();

                void working_indicator(const bool enable);

                // Indicators
                pinout::indicators::JobIsDoing _job_indicator;

                // Buttons
                pinout::buttons::StartStopButton _start_stop_button;

                // States
                volatile bool _enable_process = false;
        };

} // namespace service
} // namespace molding
} // namespace yarildo
