#pragma once
#include <general/gpio.hpp>

#include <cstdint>

#include <niietcm4.h>

namespace yarildo {

namespace molding {

namespace pinout {

namespace hydraulics {
    
        using namespace yarildo::molding::io::gpio;

        using InjectValvePull   = Pin<NT_GPIOC_BASE, (1 << 13)>; // FIXME выставить верный порт и пин
        using InjectValvePush   = Pin<NT_GPIOC_BASE, (1 << 13)>; // FIXME выставить верный порт и пин

        using MoldLockValvePull = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин
        using MoldLockValvePush = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин

        using InjectLockPush    = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин

        using CommonValve       = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин

} // hydraulics

namespace sensors {

        using namespace yarildo::molding::io::gpio;

        using InjectionCylinderLockSensor          = Pin<NT_GPIOC_BASE, (1 << 13)>; // FIXME выставить верный порт и пин
        using InjectionCylinderUnlockSensor        = Pin<NT_GPIOC_BASE, (1 << 13)>; // FIXME выставить верный порт и пин

        using InjectionLockingCylinderLockSensor   = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин
        using InjectionLockingCylinderUnlockSensor = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин

        using MoldLockingCylinderLockSensor        = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин
        using MoldLockingCylinderUnlockSensor      = Pin<NT_GPIOC_BASE, (1 << 14)>; // FIXME выставить верный порт и пин

} // sensors

namespace indicators {

        using namespace yarildo::molding::io::gpio;

        using JobIsDoing = Pin<NT_GPIOC_BASE, (1 << 13)>; // FIXME выставить верный порт и пин

} // indicators

namespace buttons {

        using namespace yarildo::molding::io::gpio;

        using StartStopButton = Pin<NT_GPIOB_BASE, (1 << 12)>; // FIXME выставить верный порт и пин

} // buttons

} // pinout

} // namespace molding

} // namespace yarildo
