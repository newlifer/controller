#pragma once
#include <general/io.hpp>

#include <cstdint>

#include <niietcm4.h>

namespace yarildo {

namespace molding {

namespace io {

namespace gpio {

        template <uintptr_t PortV, IOType IOTypeV, IRQn_Type IRQTypeV, uint32_t PinsMaskV>
        struct Port
        {
                constexpr static uintptr_t port = PortV;
                constexpr static IOType io_type = IOTypeV;
                constexpr static IRQn_Type irqn_type = IRQTypeV;
                constexpr static uint32_t pins_mask = PinsMaskV;

                void init()
                {
                        GPIO_Init_TypeDef GPIOInit;
                        GPIO_StructInit(&GPIOInit);
                        if constexpr (io_type == IOType::OUTPUT) {
                                GPIOInit.GPIO_Dir = GPIO_Dir_Out;
                                GPIOInit.GPIO_Out = GPIO_Out_En;
                        }
                        else {
                                GPIOInit.GPIO_Dir = GPIO_Dir_In;
                        }

                        GPIOInit.GPIO_Pin = pins_mask;
                        GPIO_Init((NT_GPIO_TypeDef*)port, &GPIOInit);
                }

                void enable_handler()
                {
                        GPIO_ITCmd((NT_GPIO_TypeDef*)port, pins_mask, ENABLE);
                        NVIC_EnableIRQ(irqn_type);
                }
        };

        template <uintptr_t GPIOPortAddress, uint32_t PinAddress>
        struct Pin {
                static constexpr int m_pin_address = PinAddress;
                static constexpr uintptr_t m_gpio_port = GPIOPortAddress;

                constexpr void enable()
                {
                        GPIO_SetBits((NT_GPIO_TypeDef*)m_gpio_port, m_pin_address);
                }

                constexpr void disable()
                {
                        GPIO_ClearBits((NT_GPIO_TypeDef*)m_gpio_port, m_pin_address);
                }

                constexpr uint32_t read()
                {
                        return GPIO_ReadBit((NT_GPIO_TypeDef*)m_gpio_port, m_pin_address);
                }
        };

} // namespace gpio

} // namespace io

} // namespace molding

} // namespace yarildo
