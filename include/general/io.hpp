#pragma once

namespace yarildo {

namespace molding {

namespace io {

        enum class IOType {
                INPUT = 0,
                OUTPUT = 1
        };

} // namespace io

} // namespace molding

} // namespace yarildo
