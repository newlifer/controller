/**
 */
#pragma once
#include <general/gpio.hpp>

#include <cstdint>
#include <cassert>
#include <functional>

namespace yarildo
{
namespace molding
{
namespace service
{
namespace drives
{
        namespace io = yarildo::molding::io;

        /** @brief Управление гидро/пневмноцилиндром. Перемещение, определение позиции.
         * 
         * @tparam ForwardPin Пин, включающий клапан на движение вперёд.
         * @tparam BackwardPin Пин, включающий клапан на движение назад.
         * @tparam MaxStroke Максимальный ход штока в мм.
         *
         */
        template <auto ForwardPin, auto BackwardPin, uint32_t MaxStroke = 0>
        class Cylinder {

                static constexpr decltype(MaxStroke) _max_stroke = MaxStroke;

                decltype(ForwardPin) _forward_pin = ForwardPin;
                decltype(BackwardPin) _backward_pin = BackwardPin;

        public:

                Cylinder()
                {
                }

                /** @brief Перемещает шток цилинлра на N мм. вперёд.
                 * @details Если расстояние не указано, то шток переместится до конца.
                 *
                 * @returns bool Удалось отослать команду, иначе шток уже находится на нужном положении.
                 */
                inline bool start_move_forward(const uint32_t mm = 0)
                {
                        assert((mm < _max_stroke));

                        bool ret = false;
                        if(this->is_on_front_end_position())
                                return ret;

                        if(mm && this->position() == mm)
                                return ret;

                        this->_backward_pin.disable();
                        this->_forward_pin.enable();

                        if(mm)
                                this->_required_position = mm;

                        ret = true;
                        return ret;
                }

                /** @brief Перемещает шток цилинлра на N мм. обратно.
                 * @details Если расстояние не указано, то шток переместится до конца.
                 *
                 * @returns bool Удалось отослать команду, иначе шток уже находится на нужном положении.
                 */
                inline bool start_move_backward(const uint32_t mm = 0)
                {
                        assert((mm < _max_stroke));

                        bool ret = false;
                        if(this->is_on_back_end_position())
                                return ret;

                        if(mm && this->position() == mm)
                                return ret;

                        if(mm)
                                this->_required_position = mm;

                        this->_forward_pin.disable();
                        this->_backward_pin.enable();

                        ret = true;
                        return ret;
                }

                /** @brief Останавливает шток. Выключает оба управляющих клапана.
                 */
                inline void stop()
                {
                        this->_forward_pin.disable();
                        this->_backward_pin.disable();  
                }

                /** @brief Возвращает признак того, что шток находится в крайнем заднем положении.
                 *
                 * @returns bool
                 */
                inline bool is_on_back_end_position() const
                {
                        return _is_at_front_end;
                }

                /** @brief Возвращает признак того, что шток находится в крайнем переднем положении.
                 *
                 * @returns bool
                 */
                inline bool is_on_front_end_position() const
                {
                        return _is_at_back_end;
                }

                /** @brief Возвращает текущее положение штока.
                 * @warning Если цилиндр не оборудован датчиком положения штока,
                 * то вернётся 0.
                 *
                 * @returns uint32_t на сколько выдвинут шток в мм
                 */
                inline uint32_t position() const
                {
                        return _current_position;
                }

                /** @brief возвращает функцию-обработчик
                 */
                std::function<void()> get_handler()
                {
                        return [this] () {
                                this->_do();
                        };
                }

        private:

                void _do()
                {
                        // Первым делом определяем текущее положение штока
                        this->_determine_position();

                        // Если шток по датчику вышел дальше его расчётного,
                        // то что-то пошло не так — останавливаем
                        if(_current_position > _max_stroke)
                                stop();

                        // Дошли до позиции или до края — останавливаем
                        if(this->_forward_pin.read() && !_is_at_front_end) {
                                if(!_required_position && _is_at_front_end)
                                        stop();
                                else if(_required_position && _required_position >= _current_position)
                                        stop();
                        }

                        // Дошли до позиции или до края — останавливаем
                        if(this->_backward_pin.read() && !_is_at_back_end) {
                                if(!_required_position && _is_at_back_end)
                                        stop();
                                else if(_required_position && _required_position < _current_position)
                                        stop(); 
                        }
                }

                /** @brief Определяет текущее положение штока: в мм, а так же находится ли шток крайних положениях.
                 */
                void _determine_position()
                {
                        // FIXME
                }

                uint32_t _current_position = 0;
                uint32_t _required_position = 0;

                bool _is_at_front_end = false;
                bool _is_at_back_end = false;
        };

} // namespace drives
} // namespace service
} // namespace molding
} // namespace yarildo
