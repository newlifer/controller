/*==============================================================================
 * Управление периферией на плате LDM-HELPER-K1921VK01T
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 * НИИЭТ, Александр Дыхно <dykhno@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "bsp.h"

//-- Private variables ---------------------------------------------------------
static volatile uint32_t btn_press_event = 0;

//-- Functions -----------------------------------------------------------------
void BSP_LED_Init()
{
    LED0_2_PORT->OUTENSET = LED0_2_PIN_MSK;
    NT_COMMON_REG->LED0_2_PORT_DEN |= LED0_2_PIN_MSK;
    LED3_5_PORT->OUTENSET = LED3_5_PIN_MSK;
    NT_COMMON_REG->LED3_5_PORT_DEN |= LED3_5_PIN_MSK;
    LED6_7_PORT->OUTENSET = LED6_7_PIN_MSK;
    NT_COMMON_REG->LED6_7_PORT_DEN |= LED6_7_PIN_MSK;    
}

void BSP_LED_On(uint32_t leds)
{
    if (leds & 0x07) LED0_2_PORT->DATAOUT |=  (leds & 0x07) << LED0_POS;
    if (leds & 0x38) LED3_5_PORT->DATAOUT |= ((leds & 0x38) >> 3) << LED3_POS;
    if (leds & 0xC0) LED6_7_PORT->DATAOUT |= ((leds & 0xC0) >> 6) << LED6_POS;    
}

void BSP_LED_Off(uint32_t leds)
{
    if (leds & 0x07) LED0_2_PORT->DATAOUT &= ~ ((leds & 0x07) << LED0_POS);
    if (leds & 0x38) LED3_5_PORT->DATAOUT &= ~(((leds & 0x38) >> 3) << LED3_POS);
    if (leds & 0xC0) LED6_7_PORT->DATAOUT &= ~(((leds & 0xC0) >> 6) << LED6_POS);        
}

void BSP_Btn_Init()
{
    // Настройка выводов
    //SB1
    BTN_SB1_PORT->OUTENCLR = BTN_SB1_PIN_MSK;
    NT_COMMON_REG->BTN_SB1_PORT_EN |= BTN_SB1_PIN_MSK;
    //SB2
    BTN_SB2_PORT->OUTENCLR = BTN_SB2_PIN_MSK;
    NT_COMMON_REG->BTN_SB2_PORT_EN |= BTN_SB2_PIN_MSK;
    //SB3
    BTN_SB3_PORT->OUTENCLR = BTN_SB3_PIN_MSK;
    NT_COMMON_REG->BTN_SB3_PORT_EN |= BTN_SB3_PIN_MSK;
    //SB4
    BTN_SB4_PORT->OUTENCLR = BTN_SB4_PIN_MSK;
    NT_COMMON_REG->BTN_SB4_PORT_EN |= BTN_SB4_PIN_MSK;
    
#ifdef BTN_USE_IRQ    
    NVIC_EnableIRQ(BTN_IRQ_N);
#endif
}

/* Определение "кода" по нажатым кнопкам */
BTNCode BSP_Btn_GetKey(void)
{
    uint32_t i, sKey;
    uint8_t data[BTN_COUNT];

    for(i=0;i<BTN_COUNT;i++)
    {
        data[i] = 1;
    }

    // Собираем данные с кнопок в массив с инверсией  значений (1 - нажата, 0 - не нажата)
    if(BTN_SB1_PORT->DATA & BTN_SB1_PIN_MSK) data[0] = 0;   /* SW0      E15*/
    if(BTN_SB2_PORT->DATA & BTN_SB2_PIN_MSK) data[1] = 0;   /* SW1      G4*/
    if(BTN_SB3_PORT->DATA & BTN_SB3_PIN_MSK) data[2] = 0;   /* SW2      G8*/
    if(BTN_SB4_PORT->DATA & BTN_SB4_PIN_MSK) data[3] = 0;   /* SW3      G9*/

    // Суммируем состояния кнопок
    sKey=0;
    for(i=0;i<BTN_COUNT;i++)
    {
        sKey = sKey + data[i];
    }

    if(sKey == 0) /* NOKEY    */
    {
        return NOKEY; 
    }
    else if(sKey > 1)  /* MULTIPLE */
    {
        return MULTIPLE;
    }

    // Если нажата только одна кнопка, то распознаем её
    for(i=0;i<BTN_COUNT;i++)
    {
        if(data[i] == 1)
        {
            return ((BTNCode)(i+1));
        }
    }

    // На случай, если не удалось никак обработать выше
    return NOKEY;
}

uint32_t BSP_Btn_IsPressed()
{
    uint32_t tmp;
    if (btn_press_event) {
        tmp = btn_press_event;
        btn_press_event = 0;
        return tmp;
    } else
        return 0;
}


//-- IRQ handlers --------------------------------------------------------------
void BTN_IRQ_HANDLER()
{
    BTN_PORT->INTSTATUS = BTN_PIN_MSK;
    btn_press_event = BSP_Btn_GetKey();
}