/*==============================================================================
 * Определения для периферии платы LDM-HELPER-K1921VK01T
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 * НИИЭТ, Александр Дыхно <dykhno@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

#ifndef BSP_H
#define BSP_H

#ifdef __cplusplus
extern "C" {
#endif

//-- Includes ------------------------------------------------------------------
#include "K1921VK01T.h"
#include <stdio.h>

//Buttons
#define BTN_PORT NT_GPIOF
#define LED_PORT NT_GPIOG

#define BTN_PIN_MSK 0x00C0

//-- Defines -------------------------------------------------------------------
//LEDs LED0(PC13) LED1(PC14) LED2(PC15) LED3(PD13) LED4(PD14) LED5(PD15)  LED6(PE2) LED7(PE3) 
#define LED0_2_PORT NT_GPIOC
#define LED0_2_PORT_DEN GPIODENC
#define LED0_2_PIN_MSK 0xE000
#define LED3_5_PORT NT_GPIOD
#define LED3_5_PORT_DEN GPIODEND
#define LED3_5_PIN_MSK 0xE000	
#define LED6_7_PORT NT_GPIOE
#define LED6_7_PORT_DEN GPIODENE
#define LED6_7_PIN_MSK 0xE000		
#define LED_MAX  8	
#define LED0_POS 13
#define LED1_POS 14
#define LED2_POS 15
#define LED3_POS 13
#define LED4_POS 14
#define LED5_POS 15
#define LED6_POS 2
#define LED7_POS 3
#define LED0_MSK (1 << LED0_POS)
#define LED1_MSK (1 << LED1_POS)
#define LED2_MSK (1 << LED2_POS)
#define LED3_MSK (1 << LED3_POS)
#define LED4_MSK (1 << LED4_POS)
#define LED5_MSK (1 << LED5_POS)
#define LED6_MSK (1 << LED6_POS)
#define LED7_MSK (1 << LED7_POS)

//Buttons
//Button SW0 E15
#define BTN_SB1_PORT NT_GPIOE
#define BTN_SB1_PORT_EN GPIODENE
#define BTN_SB1_PIN_POS 15
#define BTN_SB1_PIN_MSK (1 << BTN_SB1_PIN_POS)

//Button SW1 G4
#define BTN_SB2_PORT NT_GPIOG
#define BTN_SB2_PORT_EN GPIODENG
#define BTN_SB2_PIN_POS 4
#define BTN_SB2_PIN_MSK (1 << BTN_SB2_PIN_POS)

//Button SW2 G8
#define BTN_SB3_PORT NT_GPIOG
#define BTN_SB3_PORT_EN GPIODENG
#define BTN_SB3_PIN_POS 8
#define BTN_SB3_PIN_MSK (1 << BTN_SB3_PIN_POS)

//Button SW3 G9
#define BTN_SB4_PORT NT_GPIOG
#define BTN_SB4_PORT_EN GPIODENG
#define BTN_SB4_PIN_POS 9
#define BTN_SB4_PIN_MSK (1 << BTN_SB4_PIN_POS)

#define BTN_USE_IRQ
#define BTN_IRQ_N GPIOG_IRQn // GPIOE_IRQn?
#define BTN_IRQ_HANDLER GPIOG_IRQHandler

#define BTN_COUNT	4	

/* "Коды клавиш" */
typedef enum {
	NOKEY	 = 0,
    SW0      = 1,
    SW1      = 2,
    SW2      = 3,
    SW3      = 4,
    MULTIPLE = 5,    
    NUM_BTN_CODES    
} BTNCode;

//-- Functions -----------------------------------------------------------------
void BSP_LED_Init(void);
void BSP_LED_On(uint32_t leds);
void BSP_LED_Off(uint32_t leds);

void BSP_Btn_Init(void);
uint32_t BSP_Btn_IsPressed(void);
BTNCode BSP_Btn_GetKey(void);

// BTN functions
__STATIC_INLINE uint8_t BSP_BTN_KEY_PRESSED(uint8_t Key)
{
    return (BSP_Btn_GetKey() == Key);
}

__STATIC_INLINE void BSP_BTN_WAIT_UNTIL_KEY_PRESSED(uint8_t Key)
{
    while(!BSP_BTN_KEY_PRESSED(Key));
}

__STATIC_INLINE void BSP_BTN_WAIT_UNTIL_KEY_RELEASED(uint8_t Key)
{
    while(BSP_BTN_KEY_PRESSED(Key));
}

__STATIC_INLINE void BSP_BTN_WAIT_UNTIL_ANY_KEY(void)
{
    while(BSP_Btn_GetKey() == NOKEY);
}

// Others button functions
typedef enum {
    BSP_Btn_State_None,
    BSP_Btn_State_SB2,
    BSP_Btn_State_SB3,
    BSP_Btn_State_Both
} BSP_Btn_State_TypeDef;


void bsp_buttons_init(void);
// BSP_Btn_State_TypeDef bsp_button_get_state();
uint32_t bsp_button_get_state();


#ifdef __cplusplus
}
#endif

#endif // BSP_H
