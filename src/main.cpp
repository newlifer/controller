#include <system_K1921VK01T.h>
#include <niietcm4.h>

#include <application.hpp>

#define LED0_PORT       NT_GPIOC
#define LED0_PIN_MASK   (1 << 13)

#define LED1_PORT       NT_GPIOC
#define LED1_PIN_MASK   (1 << 14)

#define BTN0_PORT       NT_GPIOB
#define BTN0_PIN_MASK   (1 << 12) // пин 68, ака кнопка SA4


void init_peripheral()
{
        GPIO_Init_TypeDef GPIOInit;
        GPIO_StructInit(&GPIOInit);
        // GPIOInit.GPIO_Dir = GPIO_Dir_Out;
        // GPIOInit.GPIO_Out = GPIO_Out_En;

        /* GPIOC */
        // GPIOInit.GPIO_Pin = LED0_PIN_MASK | LED1_PIN_MASK;
        // GPIO_Init(NT_GPIOC, &GPIOInit);

        /* GPIOB */
        GPIO_StructInit(&GPIOInit);
        GPIOInit.GPIO_Dir = GPIO_Dir_In;

        GPIOInit.GPIO_Pin = BTN0_PIN_MASK;
        GPIO_Init(NT_GPIOB, &GPIOInit);

        GPIO_ITCmd(NT_GPIOB, BTN0_PIN_MASK, ENABLE);
        NVIC_EnableIRQ(GPIOB_IRQn);


        // Инициализируем время и дату
        // RTC_Time_TypeDef RTC_TimeStruct;
        // RTC_Date_TypeDef RTC_DateStruct;

        // RTC_TimeStruct.RTC_Psecond = 0;
        // RTC_TimeStruct.RTC_Second = 0;
        // RTC_TimeStruct.RTC_Minute = 1;
        // RTC_TimeStruct.RTC_Hour = 12;
        // RTC_DateStruct.RTC_Weekday = RTC_Weekday_Friday;
        // RTC_DateStruct.RTC_Day = 0x4;
        // RTC_DateStruct.RTC_Month = RTC_Month_December;
        // RTC_DateStruct.RTC_Year = 0x15;

        // RTC_SetTime(RTC_Format_BCD, &RTC_TimeStruct);
        // RTC_SetDate(RTC_Format_BCD, &RTC_DateStruct);
}


int main()
{
        using namespace yarildo::molding;
        service::Application::instance().init_peripheral();

        while(1) {
                service::Application::instance().cycle();
        }

        return 0;
}

extern "C" void GPIOB_IRQHandler()
{
        yarildo::molding::service::Application::instance().gpio_b_handle();   
}
