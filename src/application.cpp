#include <system_K1921VK01T.h>
#include <niietcm4.h>

#include <application.hpp>


#include <general/io.hpp>
#include <general/gpio.hpp>

#include <drives/cylinder.hpp>

#include <gpio_pins.hpp>

namespace yarildo
{
namespace molding
{
namespace service
{

void Application::init_peripheral()
{
        init_gpio_output();
}

void Application::init_gpio_output()
{
        // hydraulics GPIO init
        // {
        //         using namespace yarildo::molding::io;
        //         using namespace yarildo::molding::pinout::hydraulics;

        //         constexpr uint32_t output_pin_masks =
        //                 InjectValvePull::m_pin_address   ||
        //                 InjectValvePush::m_pin_address   ||
        //                 MoldLockValvePull::m_pin_address ||
        //                 MoldLockValvePush::m_pin_address ||
        //                 InjectLockPush::m_pin_address    ||
        //                 CommonValve::m_pin_address;

        //         gpio::Port<NT_GPIOC_BASE, IOType::OUTPUT, GPIOC_IRQn, output_pin_masks> c_port;
        //         c_port.init();
        // }

        // indicators GPIO init
        {
                using namespace yarildo::molding::io;
                using namespace yarildo::molding::pinout::indicators;

                constexpr uint32_t output_pin_masks = JobIsDoing::m_pin_address;

                gpio::Port<NT_GPIOC_BASE, IOType::OUTPUT, GPIOC_IRQn, output_pin_masks> c_port;
                c_port.init();
        }

        // buttons GPIO init
        {
                using namespace yarildo::molding::io;
                using namespace yarildo::molding::pinout::buttons;

                constexpr uint32_t pin_masks = StartStopButton::m_pin_address;

                gpio::Port<NT_GPIOB_BASE, IOType::INPUT, GPIOB_IRQn, pin_masks> b_port;
                b_port.init();
                b_port.enable_handler();
        }
}

int Application::cycle()
{
        working_indicator(_enable_process);
        return 0;
}

void Application::gpio_b_handle()
{
        _enable_process = true;
}

void Application::init_clock()
{
        // Инициализируем время и дату
        RTC_Time_TypeDef RTC_TimeStruct;
        RTC_Date_TypeDef RTC_DateStruct;

        RTC_TimeStruct.RTC_Psecond = 0;
        RTC_TimeStruct.RTC_Second = 0;
        RTC_TimeStruct.RTC_Minute = 1;
        RTC_TimeStruct.RTC_Hour = 12;
        RTC_DateStruct.RTC_Weekday = RTC_Weekday_Friday;
        RTC_DateStruct.RTC_Day = 0x4;
        RTC_DateStruct.RTC_Month = RTC_Month_December;
        RTC_DateStruct.RTC_Year = 0x15;

        RTC_SetTime(RTC_Format_BCD, &RTC_TimeStruct);
        RTC_SetDate(RTC_Format_BCD, &RTC_DateStruct);
}

void Application::working_indicator(const bool enable)
{
        if(enable)
                _job_indicator.enable();
        else
                _job_indicator.disable();
}

} // namespace service
} // namespace molding
} // namespace yarildo
